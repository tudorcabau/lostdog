package com.lostdog.android.lostdog.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.lostdog.android.lostdog.R;
import com.lostdog.android.lostdog.prefs.UserPrefManager;
import com.lostdog.android.lostdog.adapters.SpinnerAdapter;
import com.lostdog.android.lostdog.model.AdvertismentModel;
import com.mvc.imagepicker.ImagePicker;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Advertisment extends AppCompatActivity {
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    FirebaseStorage storageRef ;
    StorageReference storageReference;
    private AdvertismentModel advertismentModel;
    private boolean titleEdit = false , imageAdded = false, sizeSelected = false, rasaSelected = false, zonaSelected = false, descriptionAdded = false,telefonAdded = false,telefonValid = false;
    private EditText description,title,telefon;
    private boolean userIsInteracting;
    private String lostFoundType = null;
    private String userId = "00";
    private View view;
    DatabaseReference pushedPostRef;
    ImageButton imageButton ;
    private static final int RC_PHOTO_PICKER = 234;

    //ImagePicker imagePicker;

    private byte[] dataImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisment);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        lostFoundType = getIntent().getStringExtra("lost_found");
        userId = getIntent().getStringExtra("user_id");
        Log.d( "onCreate: ",userId);
        if(userId==null)
            userId="00";
        advertismentModel = new AdvertismentModel();
        advertismentModel.setType(lostFoundType);
        Log.d("onCreate: ",lostFoundType);
        view = findViewById(android.R.id.content);
        description = (EditText) view.findViewById(R.id.description_edit_text);
        title = (EditText) view.findViewById(R.id.advertisment_title_type);
        telefon = (EditText) view.findViewById(R.id.telefon_number);
        title.setHint((lostFoundType + ": "));
        imageButton = (ImageButton) findViewById(R.id.add_image_advertisment);
        ImagePicker.setMinQuality(600, 600);


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference().child("advertisements").child(lostFoundType);
        storageRef = FirebaseStorage.getInstance();
        storageReference = storageRef.getReference().child("advertisments_photos");
        //pushedPostRef = databaseReference.push();


        populateSpinners(view);
        UserPrefManager userPref = new UserPrefManager(getApplicationContext());
        Log.d( "setPhoneNumber: ","userPhone" +  userPref.getUserPhone());
        telefon.setText(userPref.getUserPhone());
        descriptionText();

        //populateZone();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        userIsInteracting = true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK ) {
            Bitmap bitmap = ImagePicker.getImageFromResult(this, requestCode, resultCode, data);
            Log.d( "onActivityResult: ","Image Successfully taken ");
            imageButton.setImageBitmap(bitmap);
            //advertismentModel.setImage(bitmap);
            imageAdded = true;
            ;
            //Uri uri = data.getData();



            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            dataImage = baos.toByteArray();

            //UploadTask uploadTask = photoRef.putBytes(dataImage);
            String imageName = advertismentModel.getId()+advertismentModel.getDate() + advertismentModel.getRasa();
            StorageReference photoRef = storageReference.child(imageName);
            photoRef.putBytes(dataImage).addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Uri downLoadUrl = taskSnapshot.getDownloadUrl();

                    advertismentModel.setPhotoUrl(downLoadUrl.toString());
                    Log.d("onActvityResult", "onSuccess: downloadURL " + downLoadUrl);



                }
            });

        }
    }

    public void onPickImage(View view) {
        // Click on image button
        ImagePicker.pickImage(this, "Selectati o imagine: ");

    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        ImageButton imageButton1 = null ;
        if (checked) {
            sizeSelected = true;

            switch (view.getId()) {
                case R.id.breed_small:
//                    imageButton1 = (ImageButton) findViewById(R.id.breed_small);
                    advertismentModel.setTalie("Mica");
                    break;
                case R.id.breed_med:
//                    imageButton1 = (ImageButton) findViewById(R.id.breed_med);
                    advertismentModel.setTalie("Medie");
                    break;

                case R.id.breed_large:
//                    imageButton1 = (ImageButton) findViewById(R.id.breed_large);
                    advertismentModel.setTalie("Mare");
                    break;
//            default:
//                imageButton1 = (ImageButton) findViewById(R.id.breed_small);

            }
        }
    }

    private void populateSpinners(View view){
        final Spinner spinnerRasa = (Spinner) view.findViewById(R.id.spinner_rasa);
        final Spinner spinnerZona = (Spinner) view.findViewById(R.id.spinner_zona);
        String[] zone = {"Selecteaza Zona","Alta Zona","Buna Ziua","Centru","Dambul Rotund","Gara","Gheorgheni","Gruia","Iris","Intre Lacuri","Manastur","Marasti","Zorilor"};
        String[] rase = {"Selecteaza Rasa","Metis","Labrador","Cocker Spaniel","Ciobanesc German","Golden Retriever","Boxer","Boxer Terrier","Beagle","Yorkshire Terrier"};

        List<String> zoneList = new ArrayList<String>(zone.length);
        zoneList.addAll(Arrays.asList(zone));

        List<String> raseList = new ArrayList<String>(rase.length);
        raseList.addAll(Arrays.asList(rase));

        SpinnerAdapter areasAdapter = new SpinnerAdapter(Advertisment.this, R.layout.spinner_item, zoneList);
        spinnerZona.setAdapter(areasAdapter);

        SpinnerAdapter raseAdapter = new SpinnerAdapter(Advertisment.this,  R.layout.spinner_item, raseList);
        spinnerRasa.setAdapter(raseAdapter);

        spinnerZona.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
            String selectedItem = parentView.getItemAtPosition(position).toString();
            if (userIsInteracting) {
                advertismentModel.setZona(selectedItem);
                zonaSelected = true;
                createTitlu();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parentView) {
            //do nothing
        }

    });
        spinnerRasa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String selectedItem = parentView.getItemAtPosition(position).toString();
                if (userIsInteracting) {
                    advertismentModel.setRasa(selectedItem);
                    rasaSelected = true;
                    createTitlu();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                //do nothing
            }
        });

    }
    private void setPhoneNumber(){
        UserPrefManager userPref = new UserPrefManager(getApplicationContext());
        Log.d( "setPhoneNumber: ", userPref.getUserPhone());
        telefon.setText(userPref.getUserPhone());
    }
    private void descriptionText(){



        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            advertismentModel.setDescrition(s.toString());
                Log.d("description", "afterTextChanged: ");
                descriptionAdded = true;

            }
        });

        title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("title", "afterTextChanged: ");
                advertismentModel.setTitle(s.toString());
                titleEdit = true;
            }
        });

        telefon.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>9){
                    advertismentModel.setTelefon(s.toString());
                    telefonValid = true;
                }
            }
        });


    }

    protected Rect getLocationOnScreen(View mEditText) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mEditText.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();

        return mRect;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        boolean handleReturn = super.dispatchTouchEvent(ev);

        View view = getCurrentFocus();

        int x = (int) ev.getX();
        int y = (int) ev.getY();

        if (view instanceof EditText) {
           ((EditText) view).setCursorVisible(true);
            View innerView = getCurrentFocus();

            if (ev.getAction() == MotionEvent.ACTION_UP &&
                    !getLocationOnScreen(innerView).contains(x, y)) {

                InputMethodManager input = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                input.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
                ((EditText) view).setCursorVisible(false);
                //searchBar = (EditText) findViewById(R.id.search_bar_id);
                //searchBar.clearFocus();

            }

        }

        return handleReturn;
    }
public void posteaza(View view){

    Calendar c = Calendar.getInstance();
    Date date = c.getTime();
    advertismentModel.setDate(date);
    if(userId != null) {
        advertismentModel.setId(userId);
    }
    if(setWarnings()){


        Log.d( "posteaza: ","photoUrl: " + advertismentModel.getPhotoUrl());
        //advertismentModel.setPhotoUrl("lala");

        pushedPostRef = databaseReference.push();
        String key = pushedPostRef.getKey();
        advertismentModel.setKey(key);
        pushedPostRef.setValue(advertismentModel);
        Intent loginIntent = new Intent(Advertisment.this,SearchActivity.class);
        loginIntent.putExtra("lost_found",lostFoundType);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        Advertisment.this.startActivity(loginIntent);
    }
}
private boolean setWarnings(){
    view.findViewById(R.id.talie_warning).setVisibility(View.GONE);
    view.findViewById(R.id.rasa_warning).setVisibility(View.GONE);
    view.findViewById(R.id.zona_warning).setVisibility(View.GONE);
    view.findViewById(R.id.descriere_warning).setVisibility(View.GONE);

    if(!imageAdded){
        findViewById(R.id.image_warning).setVisibility(View.VISIBLE);

    }

    if(!telefonValid)
        findViewById(R.id.telefon_warning).setVisibility(View.VISIBLE);
    if(!sizeSelected)
        findViewById(R.id.talie_warning).setVisibility(View.VISIBLE);
    if(!rasaSelected)
        findViewById(R.id.rasa_warning).setVisibility(View.VISIBLE);
    if(!zonaSelected)
        findViewById(R.id.zona_warning).setVisibility(View.VISIBLE);
    if(!descriptionAdded)
        findViewById(R.id.descriere_warning).setVisibility(View.VISIBLE);

    return (zonaSelected && rasaSelected && sizeSelected && descriptionAdded);
}

private void createTitlu(){
    StringBuilder sb =  new StringBuilder();
    sb.append(lostFoundType+" ");
    sb.append(advertismentModel.getRasa());
    if(sizeSelected){
    sb.append(" De Talie: ");
    sb.append(advertismentModel.getTalie());
    }
    if(zonaSelected){
        findViewById(R.id.zona_warning).setVisibility(View.VISIBLE);
    }
    sb.append(" In Zona: ");
    sb.append(advertismentModel.getZona());

    title.setText(sb);
}

}
