package com.lostdog.android.lostdog.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.lostdog.android.lostdog.R;
import com.lostdog.android.lostdog.utils.Utils;
import com.lostdog.android.lostdog.model.AdvertismentModel;

import java.text.SimpleDateFormat;

/**
 * Created by cabau on 1/7/2018.
 */

public class ClickedAdvertisment extends FragmentActivity{
    private View view;
    private AdvertismentModel postedAdvertismentModel ;
    private TextView title,rasa,zona,descriere;
    private ImageView postedImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posted_advertisment);

        postedAdvertismentModel = Utils.getClickedAdvertisemntModel();

        view = findViewById(android.R.id.content).getRootView();
        title = (TextView) view.findViewById(R.id.posted_title);
        rasa =(TextView) view.findViewById(R.id.posted_rasa);
        zona = (TextView) view.findViewById(R.id.posted_zona);
        descriere = (TextView) view.findViewById(R.id.posted_description);
        postedImage = (ImageView) view.findViewById(R.id.posted_image_advertisment);


        initPage();


    }
private void initPage(){
    Log.d("PostedAdvertisment", "initPage: " + postedAdvertismentModel.getTalie() + postedAdvertismentModel.getTitle());
        title.setText(postedAdvertismentModel.getTitle());
        rasa.setText(postedAdvertismentModel.getRasa());
        zona.setText(postedAdvertismentModel.getZona());
        descriere.setText(postedAdvertismentModel.getDescrition());
        Glide
            .with(getApplicationContext())
            .load(postedAdvertismentModel.getPhotoUrl())
            .asBitmap()
            .into(new SimpleTarget<Bitmap>(100,100) {

                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                    //image = resource;
                    postedImage.setImageBitmap(resource);
                }
            });
        switch (postedAdvertismentModel.getTalie()){
            case "Mica":
                RadioButton small =(RadioButton) view.findViewById(R.id.posted_breed_small);
                small.setChecked(true);
            case "Medie":
                RadioButton medium = view.findViewById(R.id.posted_breed_med);
                medium.setChecked(true);
            case "Mare":
                RadioButton large = view.findViewById(R.id.posted_breed_large);
                large.setChecked(true);
        }
}
    public void smsClick(View view){
        String phone = postedAdvertismentModel.getTelefon();
        Uri uri = Uri.fromParts("sms",phone,null);
        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
        it.putExtra("sms_body", smsTextBuilder());
        startActivity(it);
    }
    public void callClick(View view){
        String phone = postedAdvertismentModel.getTelefon();
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }

    private String smsTextBuilder(){
        StringBuilder stringBuilder =new StringBuilder();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM");
        if(postedAdvertismentModel.getType().equals("Pierdut")) {
            stringBuilder.append("Buna ziua, va informez ca am detalii in legatura cu ");
            stringBuilder.append(postedAdvertismentModel.getRasa() + "-ul pierdut in zona ");
            stringBuilder.append(postedAdvertismentModel.getZona() + " la data de ");
            stringBuilder.append(df.format(postedAdvertismentModel.getDate()));
        }else {
            stringBuilder.append("Buna ziua, as dori detalii in legatura cu ");
            stringBuilder.append(postedAdvertismentModel.getRasa() + "-ul gasit in zona ");
            stringBuilder.append(postedAdvertismentModel.getZona() + " la data de ");
            stringBuilder.append(df.format(postedAdvertismentModel.getDate()));
        }
        return stringBuilder.toString();
    }


    public void clickPhoto(View view) {
        Intent fullScreenIntent = new Intent(ClickedAdvertisment.this,FullScreenPhotoActivity.class);
        fullScreenIntent.putExtra("image_url",postedAdvertismentModel.getPhotoUrl());
        ClickedAdvertisment.this.startActivity(fullScreenIntent);
    }
}
