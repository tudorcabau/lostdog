package com.lostdog.android.lostdog.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.lostdog.android.lostdog.adapters.DatePickerFragment;
import com.lostdog.android.lostdog.R;
import com.lostdog.android.lostdog.adapters.SpinnerAdapter;
import com.lostdog.android.lostdog.utils.Utils;
import com.lostdog.android.lostdog.model.AdvertismentModel;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

public class FilterActivity extends FragmentActivity implements DatePickerDialog.OnDateSetListener {
    private boolean userIsInteracting,rasaSelected = false,zonaSelected = false;
    private boolean startDateSelectedToggle = false;
    private boolean startDateSelected = false,endDateSelected = false, sizeSelected = false, smallSizeSelect = false, mediumSizeSelect = false, largeSizeSelect = false;
    private String selectedZona = "",selectedRasa = "";
    private List<AdvertismentModel> advertisementsList = new ArrayList<AdvertismentModel>();
    private Date startDate,endDate;
    private String lostFoundType;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    Query query;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_filter);
        populateSpinners(findViewById(android.R.id.content).getRootView());
        firebaseDatabase = FirebaseDatabase.getInstance();
        lostFoundType = getIntent().getStringExtra("lost_found");

        databaseReference = firebaseDatabase.getReference().child("advertisements").child(lostFoundType);
        getFireBaseData();
    }
    private void populateSpinners(View view){
        final Spinner spinnerRasa = (Spinner) view.findViewById(R.id.list_spinner_rasa);
        final Spinner spinnerZona = (Spinner) view.findViewById(R.id.list_spinner_zona);
        String[] zone = {"Selecteaza Zona","Alta Zona","Buna Ziua","Centru","Dambul Rotund","Gara","Gheorgheni","Gruia","Iris","Intre Lacuri","Manastur","Marasti","Zorilor"};
        String[] rase = {"Selecteaza Rasa","Metis","Labrador","Cocker Spaniel","Ciobanesc German","Golden Retriever","Boxer","Boxer Terrier","Beagle","Yorkshire Terrier"};

        List<String> zoneList = new ArrayList<String>(zone.length);
        zoneList.addAll(Arrays.asList(zone));

        List<String> raseList = new ArrayList<String>(rase.length);
        raseList.addAll(Arrays.asList(rase));

        SpinnerAdapter areasAdapter = new SpinnerAdapter(FilterActivity.this, R.layout.spinner_item, zoneList);
        spinnerZona.setAdapter(areasAdapter);

        SpinnerAdapter raseAdapter = new SpinnerAdapter(FilterActivity.this,  R.layout.spinner_item, raseList);
        spinnerRasa.setAdapter(raseAdapter);

        spinnerZona.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String selectedItem = parentView.getItemAtPosition(position).toString();
                if (userIsInteracting) {
                    zonaSelected = true;
                    selectedZona = selectedItem;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
        spinnerRasa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String selectedItem = parentView.getItemAtPosition(position).toString();
                if (userIsInteracting) {
                    rasaSelected = true;
                    selectedRasa = selectedItem;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

    }
    public void talieSelect(View view){
        String callingTag = view.getTag().toString();
        switch (callingTag){
            case "mica":
                sizeSelected = true;
                smallSizeSelect = true;
                break;
            case "medie":
                sizeSelected = true;
                mediumSizeSelect = true;
                break;
            case "mare":
                sizeSelected = true;
                largeSizeSelect = true;
                break;
                default:
                    break;
        }

    }
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        userIsInteracting = true;
    }
    public void pickStartDate(View view){
        startDateSelectedToggle = true;
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(),"date");
    }
    public void pickEndDate(View view){
        startDateSelectedToggle = false;
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(),"date");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar cal = new GregorianCalendar(year, month, dayOfMonth);
        setDate(cal);
    }
    private void setDate(final Calendar calendar) {

        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
        if(startDateSelectedToggle) {
            ((TextView) findViewById(R.id.filter_start_date)).setText(dateFormat.format(calendar.getTime()));
            startDate = calendar.getTime();
            startDateSelected = true;
        }
        else{
            ((TextView) findViewById(R.id.filter_end_date)).setText(dateFormat.format(calendar.getTime()));
            endDate = calendar.getTime();
            endDateSelected = true;
        }
    }
    private ArrayList<AdvertismentModel> createFilteredList(){

        List<AdvertismentModel> filteredResults;
        filteredResults = advertisementsList;
        Log.d("createFilteredLIst", "filteredResultSize: "  + filteredResults.size());
        if(zonaSelected)
            filteredResults = filteredResults.stream().filter(b -> b.getZona().equals(selectedZona)).collect(Collectors.toList());
        if(rasaSelected)
            filteredResults = filteredResults.stream().filter(b ->b.getRasa().equals(selectedRasa)).collect(Collectors.toList());
        if(sizeSelected){
            if(!smallSizeSelect)
                filteredResults = filteredResults.stream().filter(b->!b.getTalie().equals("Mica")).collect(Collectors.toList());
            if(!mediumSizeSelect)
                filteredResults = filteredResults.stream().filter(b->!b.getTalie().equals("Medie")).collect(Collectors.toList());
            if(!largeSizeSelect)
                filteredResults = filteredResults.stream().filter(b->!b.getTalie().equals("Mare")).collect(Collectors.toList());
        }


        if(startDateSelected) {
            filteredResults = filteredResults.stream().filter(b -> b.getDate().after(startDate)).collect(Collectors.toList());
        }
        if(endDateSelected) {
            filteredResults = filteredResults.stream().filter(b ->b.getDate().before(endDate)).collect(Collectors.toList());
        }

        return (ArrayList<AdvertismentModel>) filteredResults;

    }
    public void filterOnClick(View view){
        ArrayList<AdvertismentModel> refList = createFilteredList();
        Intent loginIntent = new Intent(FilterActivity.this,SearchActivity.class);

        loginIntent.putExtra("lost_found",lostFoundType);
        loginIntent.putExtra("list_is_filtered",true);
        Utils.setAdvertismentModels(refList);
        FilterActivity.this.startActivity(loginIntent);

    }
    public void getFireBaseData(){
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AdvertismentModel advertismentModel;
                for(DataSnapshot child : dataSnapshot.getChildren()) {
                    advertismentModel = child.getValue(AdvertismentModel.class);
                    advertisementsList.add(advertismentModel);
                    Log.d("createFilteredLIst", "onDataChange: "  + advertismentModel.getTitle());
                }
                Log.d("createFilteredLIst", "onDataChange: "  + advertisementsList.size());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
