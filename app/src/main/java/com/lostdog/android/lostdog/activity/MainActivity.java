package com.lostdog.android.lostdog.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.lostdog.android.lostdog.R;
import com.lostdog.android.lostdog.utils.Utils;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 1;
    private List<AuthUI.IdpConfig> providers = Arrays.asList(
            new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
            new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
            new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build()
    );
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private String lostFoundType;
    private static final int REQUEST_READ_PHONE_STATE = 1;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        lostFoundType = getIntent().getStringExtra("lost_found");
        firebaseAuth = FirebaseAuth.getInstance();
        Utils.setUserIsSignedInWithEmail(false);

//        authStateListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
//                if (firebaseUser != null) {
//                    //onSignedInInitialized(firebaseUser.getDisplayName());
//                } else {
//
////                    Intent loginIntent = new Intent(WelcomePage.this,MainActivity.class);
////                    WelcomePage.this.startActivity(loginIntent);
//                }
//            }
//        };
//        Log.d("onCreate: ", "called again");
//       firebaseAuth.addAuthStateListener(authStateListener);
    }

//    private void onSignedInInitialized(String displayName) {
//        Intent loginIntent = new Intent(MainActivity.this, Advertisment.class);
//        loginIntent.putExtra("lost_found", lostFoundType);
//        loginIntent.putExtra("user_name", displayName);
//        loginIntent.putExtra("singed_up", "true");
//        MainActivity.this.startActivity(loginIntent);
//    }

    public void login(View view) {
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setIsSmartLockEnabled(false)
                        .build(),
                RC_SIGN_IN);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RC_SIGN_IN) {
            Log.d("MainActivity ", "onActivityResult: " + "user" + firebaseAuth.getCurrentUser().getDisplayName());
            Utils.setUserIsSignedInWithEmail(true);
            FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
            Utils.setUserId(firebaseUser.getUid());
            //startActivity(new Intent(this, Advertisment.class));

        }
    }
    public void skipLogin(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this,android.R.style.Theme_Material_Dialog_Alert);
        builder.setTitle("Skip Login");
        builder.setMessage("Nu vei putea posta anunturi decat daca esti logat");
        builder.setPositiveButton("Anuleaza", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setNegativeButton("Skip Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                Intent loginIntent = new Intent(MainActivity.this,WelcomePage.class);
//                MainActivity.this.startActivity(loginIntent);
                Utils.setUserSkippedLogin(true);
                if(Utils.isUserWantsToPost()){
                    finish();
                }
                else{
                    Intent advertismentIntent = new Intent(MainActivity.this,WelcomePage.class);
                    MainActivity.this.startActivity(advertismentIntent);
                }
            }
        });
        builder.show();
    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode)
        {
            case REQUEST_READ_PHONE_STATE:
                // if request is cancelled, the result arrays are empty
                if (grantResults.length == 0 || grantResults[0] == PackageManager.PERMISSION_DENIED)
                {

                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setMessage("Pentru a inregistra un anunt, avem nevoie de aceasta permisiune");
                    alert.setPositiveButton("Ok", null);
                    alert.show();
                }

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    skipLogin(findViewById(android.R.id.content));
                }
                return;
        }
    }
}
