package com.lostdog.android.lostdog.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.lostdog.android.lostdog.R;
import com.lostdog.android.lostdog.utils.Utils;

public class NoInternetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);
    }

    public void refreshInternet(View view){
        Utils.isInternetAvailable();
        if(Utils.isInternetAvailable())
            finish();
        else {

        }
    }
}
