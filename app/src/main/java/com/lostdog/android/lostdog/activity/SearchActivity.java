package com.lostdog.android.lostdog.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.lostdog.android.lostdog.adapters.AdvertismentAdapter;
import com.lostdog.android.lostdog.R;
import com.lostdog.android.lostdog.prefs.UserPrefManager;
import com.lostdog.android.lostdog.utils.Utils;
import com.lostdog.android.lostdog.model.AdvertismentModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SearchActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    RecyclerView recyclerView;
    private List<AdvertismentModel> advertisementsList = new ArrayList<AdvertismentModel>();
    AdvertismentAdapter advertismentAdapter;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    Query query;
    private String lostFoundType;
    private boolean listIsFiltered = false;
    private ArrayList<AdvertismentModel> filteredList;
    private boolean anunturileMele = false;
    Bitmap tempImage;
    String userID;
    private static UserPrefManager userPrefManager;
    //private ConectivityManager conectivityManager;
    private NetworkInfo networkInfo;
    private boolean isConnected;
    private Bitmap tempUserPic;
    private NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        userPrefManager = new UserPrefManager(getApplicationContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        userID = Utils.getUserId();
        initActivityComm();
        Utils.isInternetAvailable();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lista Cateii Pierduti");
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);

        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        Menu menu = navigationView.getMenu();
        if(!Utils.userIsSignedInWithEmail)
            menu.getItem(0).setVisible(false);

        navigationView.setNavigationItemSelectedListener(this);

        setList();

    }




    @Override
    protected void onResume() {
        super.onResume();

        setAccountInfo(navigationView.getHeaderView(0));

        isConnected = Utils.isInternetAvailable();
        if(!isConnected) {
            Utils.setUserIsSignedInWithEmail(false);
            Intent loginIntent = new Intent(SearchActivity.this, NoInternetActivity.class);
            SearchActivity.this.startActivity(loginIntent);
        }
    }

    private void initActivityComm() {
        Intent intent = getIntent();

        if(intent.getStringExtra("anunturile_mele") != null){

//            advertisementsList.clear();
//            String userID = Utils.getUserId();
//            databaseReference = firebaseDatabase.getReference().child("advertisements").child("Pierdut").orderByChild("id").equalTo(userID).getRef();
//            getPostsForUser(databaseReference);
//            databaseReference = firebaseDatabase.getReference().child("advertisements").child("Gasit").orderByChild("id").equalTo(userID).getRef();
//            getPostsForUser(databaseReference);
            Log.d( "initActivityComm: ","activity UserID" + userID);
            anunturileMele = true;
            return;
        }
        if(getIntent().getStringExtra("lost_found").equals("Pierdut"))
            lostFoundType = "Pierdut";
        else
            lostFoundType = "Gasit";

        databaseReference = firebaseDatabase.getReference().child("advertisements").child(lostFoundType);

        if(getIntent().getBooleanExtra("list_is_filtered",false)) {
            listIsFiltered = true;
            //filteredList = (ArrayList<AdvertismentModel>) getIntent().getSerializableExtra("filtered_list");
            filteredList = Utils.getAdvertismentModels();
        }

    }

    private void getPostsForUser(DatabaseReference databaseReference) {


        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AdvertismentModel advertismentModel;
                ArrayList<AdvertismentModel> tempList = new ArrayList<AdvertismentModel>();
                for(DataSnapshot child : dataSnapshot.getChildren()) {
                    advertismentModel = child.getValue(AdvertismentModel.class);
                    tempList.add(advertismentModel);
                    Log.d("GetPostsForUser","Post UserID:" + advertismentModel.getId());
                }
                tempList = (ArrayList<AdvertismentModel>) tempList.stream().filter(b->b.getId().equals(userID)).collect(Collectors.toList());
                Log.d("GetPostsForUser","tempListSize" + tempList.size());
                advertisementsList.addAll(tempList);
                advertismentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        if(Utils.isUserIsSignedInWithEmail()){
//
//        else{
//            getMenuInflater().inflate(R.menu.menu2, menu);}

            getMenuInflater().inflate(R.menu.search, menu);
            if(!Utils.userIsSignedInWithEmail) {
                menu.getItem(0).setTitle("SignIn");
            }
            else {
                menu.getItem(0).setTitle("SignOut");
            }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.sign_out) {
            Task task = AuthUI.getInstance().signOut(this);
            Utils.setUserId("");
            Log.d("onCreate:MainActivity ","signedout " + task.isSuccessful());
            Utils.setUserIsSignedInWithEmail(false);
            return true;
        }
        if (id == R.id.sign_in) {
            Intent loginIntent = new Intent(SearchActivity.this, MainActivity.class);
            loginIntent.putExtra("lost_found",lostFoundType);
            SearchActivity.this.startActivity(loginIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void deletePost(View view){
        View rootView = (View) view.getParent() // Frame Layout
                .getParent(); // Linear Layout
        int itemPosition = recyclerView.getChildAdapterPosition(rootView);
        AdvertismentModel item =  advertisementsList.get(itemPosition);
        String key = item.getKey();
        Log.d("deletePost: ","called on " + item.getKey());
        databaseReference.child(key).removeValue();
        advertisementsList.remove(item);
        advertismentAdapter.notifyDataSetChanged();
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();
        if (id == R.id.anunturile_mele) {
            Intent loginIntent = new Intent(SearchActivity.this, SearchActivity.class);
            loginIntent.putExtra("anunturile_mele","true");
            SearchActivity.this.startActivity(loginIntent);

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void setAccountInfo(View view){

            ImageView profilePic =  view.findViewById(R.id.profile_pic_drawer);
            TextView userNameView = (TextView) view.findViewById(R.id.user_id_drawer);
            TextView userEmailView = (TextView) view.findViewById(R.id.user_email_drawer);
        if(Utils.userIsSignedInWithEmail){
            Glide.with(this)
                    .load(userPrefManager.getProfilePic())
                    .placeholder(R.drawable.account_circle)
                    .error(R.drawable.account_circle)
                    .override(200, 200)
                    .centerCrop()
                    .into(profilePic);

            String tempUserName = userPrefManager.getUserName();
            if(tempUserName != null){
                userNameView.setText(tempUserName);}
            else{
                userNameView.setText(tempUserName);
                }
            String tempUserEmail = userPrefManager.getUserEmail();
            if(tempUserEmail != null){
                userEmailView.setText(tempUserEmail);}
            else{
                userEmailView.setText(tempUserEmail);
            }
        }else {
            profilePic.setImageResource(R.drawable.account_circle);
            userNameView.setText("Nume Utilizator");
            userEmailView.setText("Email Utilizator");
        }
    }
    public void setList(){
        View view = findViewById(android.R.id.content);
        Context context = view.getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_view_advertisements);

        advertisementsList = new ArrayList<>();
        advertismentAdapter = new AdvertismentAdapter(view.getContext(), advertisementsList,recyclerView,anunturileMele);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(advertismentAdapter);
        if(listIsFiltered){
            advertisementsList.addAll(filteredList);
            Log.d("if its filtered", "setList: " + filteredList.size());
            advertismentAdapter.notifyDataSetChanged();
        }
        else if(anunturileMele){
            advertisementsList.clear();
            databaseReference = firebaseDatabase.getReference().child("advertisements").child("Pierdut");
            getPostsForUser(databaseReference);
            databaseReference = firebaseDatabase.getReference().child("advertisements").child("Gasit");
            getPostsForUser(databaseReference);
            advertismentAdapter.notifyDataSetChanged();
        }
        else {
           databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            AdvertismentModel advertismentModel;
            for(DataSnapshot child : dataSnapshot.getChildren()) {
                advertismentModel = child.getValue(AdvertismentModel.class);

                Glide
                        .with(getApplicationContext())
                        .load(advertismentModel.getPhotoUrl())
                        .asBitmap()
                        .into(new SimpleTarget<Bitmap>(100,100) {

                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                                //image = resource;
                                tempImage = resource;
                            }
                        });

                Log.d( "onDataChange: ","photoUrl " + advertismentModel.getPhotoUrl());
                advertismentModel.setImage(tempImage);
                advertisementsList.add(advertismentModel);
            }
            advertismentAdapter.notifyDataSetChanged();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });
       }


    }
    public void launchFilter(View view){
        Intent loginIntent = new Intent(SearchActivity.this,FilterActivity.class);
        loginIntent.putExtra("lost_found",lostFoundType);
        SearchActivity.this.startActivity(loginIntent);
    }

    public void openListAdvertisment(View view){
        Intent loginIntent = new Intent(SearchActivity.this,ClickedAdvertisment.class);
        SearchActivity.this.startActivity(loginIntent);

    }
}
