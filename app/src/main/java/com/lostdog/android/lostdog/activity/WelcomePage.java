package com.lostdog.android.lostdog.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.lostdog.android.lostdog.R;
import com.lostdog.android.lostdog.utils.Utils;

public class WelcomePage extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Utils.isInternetAvailable();

    }
    public void openWelcomeTwoLost(View view){
        Intent loginIntent = new Intent(WelcomePage.this,WelcomePageTwo.class);
        loginIntent.putExtra("lost_found","Pierdut");
        WelcomePage.this.startActivity(loginIntent);
    }
    public void openWelcomeTwoFound(View view){
        Intent loginIntent = new Intent(WelcomePage.this,WelcomePageTwo.class);
        loginIntent.putExtra("lost_found","Gasit");
        WelcomePage.this.startActivity(loginIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
