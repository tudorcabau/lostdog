package com.lostdog.android.lostdog.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.lostdog.android.lostdog.R;
import com.lostdog.android.lostdog.utils.Utils;

public class WelcomePageTwo extends AppCompatActivity {
String lostFoundType;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page_two);
        lostFoundType = getIntent().getStringExtra("lost_found");
        firebaseAuth = FirebaseAuth.getInstance();
        Utils.setUserSkippedLogin(false);
    }


    public void openList(View view){
        Intent loginIntent = new Intent(WelcomePageTwo.this,SearchActivity.class);
        loginIntent.putExtra("lost_found",lostFoundType);
        WelcomePageTwo.this.startActivity(loginIntent);
    }
    public void openAdvertisment(View view) {

            Utils.setUserWantsToPost(true);
            authStateListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                    if (firebaseUser != null) {
                        onSignedInInitialized(firebaseUser.getUid());
                        Utils.setUserIsSignedInWithEmail(true);
                        Utils.setUserId(firebaseUser.getUid());
                    } else {
                        onSignedOutCleanUp();
                        Utils.setUserWantsToPost(true);
                        Intent loginIntent = new Intent(WelcomePageTwo.this, MainActivity.class);
                        loginIntent.putExtra("lost_found",lostFoundType);
                        WelcomePageTwo.this.startActivity(loginIntent);
                    }
                }
            };
            Log.d("onCreate: ", "called again");
            firebaseAuth.addAuthStateListener(authStateListener);


    }
    private void onSignedOutCleanUp() {
        Utils.setUserIsSignedInWithEmail(false);
    }


    @Override
    protected void onResume() {
        super.onResume();
        //firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        firebaseAuth.removeAuthStateListener(authStateListener);

    }
    private void onSignedInInitialized(String uid) {
        Intent loginIntent = new Intent(WelcomePageTwo.this, Advertisment.class);
        loginIntent.putExtra("lost_found",lostFoundType);
        loginIntent.putExtra("user_id", uid);
        Utils.setUserId(uid);
        loginIntent.putExtra("singed_up", "true");
        WelcomePageTwo.this.startActivity(loginIntent);
    }
}
