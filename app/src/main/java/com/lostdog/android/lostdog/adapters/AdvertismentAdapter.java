package com.lostdog.android.lostdog.adapters;

/**
 * Created by cabau on 1/5/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lostdog.android.lostdog.activity.ClickedAdvertisment;
import com.lostdog.android.lostdog.R;
import com.lostdog.android.lostdog.utils.Utils;
import com.lostdog.android.lostdog.model.AdvertismentModel;

import java.text.SimpleDateFormat;
import java.util.List;

public class AdvertismentAdapter extends RecyclerView.Adapter<AdvertismentAdapter.MyViewHolder> {
    private Context context;
    private List<AdvertismentModel> advertismentModels;
    private final View.OnClickListener mOnClickListener = new MyOnClickListener();
    private RecyclerView recyclerView;
    private boolean showDelete;
    public AdvertismentAdapter(Context context, List<AdvertismentModel> advertismentModels,RecyclerView recyclerView,boolean showDelete) {
        this.context = context;
        this.advertismentModels = advertismentModels;
        this.recyclerView = recyclerView;
        this.showDelete = showDelete;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView titlu,zona, descriere,date;
        public TextView telefon;
        public ImageView thumbnail;
        public ImageButton deleteButton;

        public MyViewHolder(View view) {
            super(view);
            titlu = (TextView) view.findViewById(R.id.list_title);
            zona = (TextView) view.findViewById(R.id.list_zona);
            descriere = (TextView) view.findViewById(R.id.list_descriere);
            date = (TextView) view.findViewById(R.id.list_date);
            telefon = (TextView) view.findViewById(R.id.list_telefon);
            thumbnail = (ImageView) view.findViewById(R.id.advertisement_thumbnail);
            deleteButton = (ImageButton) view.findViewById(R.id.delete_button);
            if(showDelete)
            deleteButton.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public AdvertismentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        itemView.setOnClickListener(mOnClickListener);
        return new MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(AdvertismentAdapter.MyViewHolder holder, int position) {
        AdvertismentModel advertismentModel = advertismentModels.get(position);
        holder.titlu.setText(advertismentModel.getTitle());
        holder.zona.setText(advertismentModel.getZona());
        holder.descriere.setText(("Descriere: " + advertismentModel.getDescrition()));
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        holder.date.setText(df.format(advertismentModel.getDate()));
        String telefon = advertismentModel.getTelefon();
        if(telefon == null){
            telefon = "Telefon neadaugat";
        }
        holder.telefon.setText(telefon);
        Glide.with(context).load(advertismentModel.getPhotoUrl()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return advertismentModels.size();
    }
    @Override
    public long getItemId(int position) {
        AdvertismentModel advertismentModel = advertismentModels.get(position);
        return (advertismentModel.getTitle() + advertismentModel.getZona()).hashCode();

    }

    private class MyOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int itemPosition = recyclerView.getChildLayoutPosition(v);
            AdvertismentModel item =  advertismentModels.get(itemPosition);
            Log.d("AdvertismentAdapter", "onClick: " + item.getTalie() + item.getTitle());
            Utils.setClickedAdvertisemntModel(item);
            Intent clickedAdvertisment = new Intent(context,ClickedAdvertisment.class);
            context.startActivity(clickedAdvertisment);

        }
    }
}

