package com.lostdog.android.lostdog.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by cabau on 12/31/2017.
 */

public class SpinnerAdapter extends ArrayAdapter<String> {


    public SpinnerAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        View v = null;
        if (position == 0) {
            TextView tv = new TextView(getContext());
            tv.setHeight(0);
            tv.setVisibility(View.GONE);
            v = tv;
        }
        else {

            v = super.getDropDownView(position, null, parent);
        }
        //hide scroll
        parent.setVerticalScrollBarEnabled(false);
        return v;
    }

}
