package com.lostdog.android.lostdog.model;

import android.graphics.Bitmap;

import java.util.Date;

/**
 * Created by cabau on 1/4/2018.
 */

public class AdvertismentModel {
   // FirebaseStorage storageRef = FirebaseStorage.getInstance();
   // StorageReference storageReference = storageRef.getReference().child("advertisments_photos");

    private String title;
    private Bitmap image;
    private String talie;
    private String rasa,zona;
    private String descrition;
    private String type;
    private Date date;
    private String key;
    private String telefon = "1234567890";
    private String id;
    private String photoUrl;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Bitmap getImage() {

        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;

    }

    public String getTalie() {
        return talie;
    }

    public void setTalie(String talie) {
        this.talie = talie;
    }

    public String getRasa() {
        return rasa;
    }

    public void setRasa(String rasa) {
        this.rasa = rasa;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getDescrition() {
        return descrition;
    }

    public void setDescrition(String descrition) {
        this.descrition = descrition;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


}
