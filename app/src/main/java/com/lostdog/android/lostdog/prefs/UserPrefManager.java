package com.lostdog.android.lostdog.prefs;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by cabau on 1/13/2018.
 */

public class UserPrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;



    String profilePic;


    String userName;
    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "lostdog-userpref";
    private static final String USER_NAME = "userName";
    private static final String USER_PHOTO = "userPhoto";
    private static final String USER_EMAIL = "userEmail";
    private static final String USER_PHONE = "userPhone";

    public UserPrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public String getProfilePic() {
        return pref.getString(USER_PHOTO,"null");
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
        editor.putString(USER_PHOTO,profilePic);
        editor.commit();
    }

    public String getUserName() {
        return pref.getString(USER_NAME,"Nume Utilizator");
    }

    public void setUserName(String userName) {

        editor.putString(USER_NAME,userName);
        editor.commit();
    }
    public String getUserEmail() {
        return pref.getString(USER_EMAIL,"dummyemail@dummy.com");
    }
    public void setUserEmail(String email) {
        editor.putString(USER_EMAIL,email);
        editor.commit();
    }
    public String getUserPhone(){return pref.getString(USER_PHONE,"07400000000");}
    public void setUserPhone(String phone){
        editor.putString(USER_PHONE,phone);
        editor.commit();
    }

}
