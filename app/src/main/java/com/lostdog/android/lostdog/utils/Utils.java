package com.lostdog.android.lostdog.utils;

import android.content.Context;
import android.graphics.Bitmap;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.lostdog.android.lostdog.model.AdvertismentModel;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by cabau on 1/6/2018.
 */

public class Utils {
    public static ArrayList<AdvertismentModel> getAdvertismentModels() {
        return advertismentModels;
    }

    public static void setAdvertismentModels(ArrayList<AdvertismentModel> advertismentModels) {
        Utils.advertismentModels.clear();
        Utils.advertismentModels = advertismentModels;
    }

    public SimpleDateFormat getDate() {
        return date;
    }

    public void setDate(SimpleDateFormat date) {
        this.date = date;
    }

    private SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
    private static ArrayList<AdvertismentModel> advertismentModels = new ArrayList<>();

    public static AdvertismentModel getClickedAdvertisemntModel() {
        return clickedAdvertisemntModel;
    }

    public static void setClickedAdvertisemntModel(AdvertismentModel clickedAdvertisemntModel) {
        Utils.clickedAdvertisemntModel = clickedAdvertisemntModel;
    }

    private static AdvertismentModel clickedAdvertisemntModel  = new AdvertismentModel();

    public static String getUserId() {
        return userId;
    }

    public static void setUserId(String userId) {
        Utils.userId = userId;
    }

    private static String userId;


    private static boolean userSkippedLogin = false;

    public static boolean isUserEnteredLoginPage() {
        return userEnteredLoginPage;
    }

    public static void setUserEnteredLoginPage(boolean userEnteredLoginPage) {
        Utils.userEnteredLoginPage = userEnteredLoginPage;
    }

    private static boolean userEnteredLoginPage = false;

    public static boolean isUserSkippedLogin() {
        return userSkippedLogin;
    }

    public static void setUserSkippedLogin(boolean userSkippedLogin) {
        Utils.userSkippedLogin = userSkippedLogin;
    }

    public static boolean isUserIsSignedInWithEmail() {
        return userIsSignedInWithEmail;
    }

    public static void setUserIsSignedInWithEmail(boolean userIsSignedInWithEmail) {
        Utils.userIsSignedInWithEmail = userIsSignedInWithEmail;
    }

    public static boolean userIsSignedInWithEmail;

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    private static boolean isConnected;

    public static boolean isInternetAvailable(){
        try {
            final InetAddress address = InetAddress.getByName("www.google.com");
            Utils.isConnected = !address.equals("");
            return !address.equals("");
        } catch (UnknownHostException e) {
            // Log error
        }
        return false;
    }

    public static boolean isUserWantsToPost() {
        return userWantsToPost;
    }

    public static void setUserWantsToPost(boolean userWantsToPost) {
        Utils.userWantsToPost = userWantsToPost;
    }

private static boolean userWantsToPost;
    private static Bitmap tempImage;
    public static Bitmap getBitmapFromUrl(String photoUrl, Context context){
        Glide
                .with(context)
                .load(photoUrl)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>(100,100) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                               tempImage = resource;
                    }
                });
        return tempImage;
    }

    public static Bitmap getCurentUserPhoto() {
        return curentUserPhoto;
    }

    public static void setCurentUserPhoto(Bitmap curentUserPhoto) {
        Utils.curentUserPhoto = curentUserPhoto;
    }

    private static Bitmap curentUserPhoto;

    public static String getCurentUserName() {
        return curentUserName;
    }

    public static void setCurentUserName(String curentUserName) {
        Utils.curentUserName = curentUserName;
    }

    private static String curentUserName;
}
